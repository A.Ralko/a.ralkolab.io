window.addEventListener("DOMContentLoaded", function () {
    // Ð¡Ð¾Ð·Ð´Ð°ÐµÐ¼ ÐºÐ¾Ð½ÑÑÐ°Ð½ÑÑ
    const PRICE_1 = 14685;
    const PRICE_2 = 152512;
    const PRICE_3 = 29241;
    const EXTRA_PRICE = 0;
    const RADIO_1 = 11367;
    const RADIO_2 = 33251;
    const RADIO_3 = 64432;
    const CHECKBOX = -11450;
    // Ð¡Ð¾Ð·Ð´Ð°ÐµÐ¼ Ð¿ÐµÑÐµÐ¼ÐµÐ½Ð½ÑÐµ
    let price = PRICE_1;
    let extraPrice = EXTRA_PRICE;
    let result;
    // Ð¡Ð¾Ð·Ð´Ð°ÐµÐ¼ html ÑÐ»ÐµÐ¼ÐµÐ½ÑÑ
    let calc = document.getElementById("calc");
    let myNum = document.getElementById("number");
    let resultSpan = document.getElementById("result-span");
    let select = document.getElementById("select");
    let radiosDiv = document.getElementById("radio-div");
    let radios = document.querySelectorAll("#radio-div input[type=radio]");
    let checkboxDiv = document.getElementById("check-div");
    let checkbox = document.getElementById("checkbox");
    // Ð¤ÑÐ½ÐºÑÐ¸Ñ Ð¾Ð±ÑÐ°Ð±Ð¾ÑÐºÐ¸ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ð¹ Ð² select
    select.addEventListener("change", function (event) {
        // ÐÑÐ±ÑÐ°Ð½Ð½ÑÐ¹ ÑÐ»ÐµÐ¼ÐµÐ½Ñ option
        let option = event.target;
        // Ð¡Ð¼Ð¾ÑÑÐ¸Ð¼ ÐºÐ°ÐºÐ¾Ð¹ Ð²ÑÐ±ÑÐ°Ð½ Ð¸ Ð¾ÑÐ¾Ð±ÑÐ°Ð¶Ð°ÐµÐ¼ ÑÐ»ÐµÐ¼ÐµÐ½ÑÑ
        if (option.value === "1") {
            // ÐÐºÐ»ÑÑÐ°ÐµÐ¼ ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÐ¸ Ð¸ Ð²ÑÐºÐ»ÑÑÐ°ÐµÐ¼ ÑÐµÐºÐ±Ð¾ÐºÑ
            radiosDiv.classList.add("d-none");
            checkboxDiv.classList.add("d-none");
            // ÐÐ·Ð¼ÐµÐ½ÑÐµÐ¼ Ð´Ð¾Ð¿. ÑÐµÐ½Ñ
            extraPrice = EXTRA_PRICE;
            // ÐÐ·Ð¼ÐµÐ½ÑÐµÐ¼ ÑÐµÐ½Ñ Ð·Ð° ÑÑÑÐºÑ
            price = PRICE_1;
        }
        if (option.value === "2") {
            radiosDiv.classList.remove("d-none");
            checkboxDiv.classList.add("d-none");
            extraPrice = RADIO_1;
            price = PRICE_2;
            // Ð¡Ð±ÑÐ°ÑÑÐ²Ð°ÐµÐ¼ ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÑ
            document.getElementById("radio1").checked = true;
        }
        if (option.value === "3") {
            checkboxDiv.classList.remove("d-none");
            radiosDiv.classList.add("d-none");
            extraPrice = EXTRA_PRICE;
            price = PRICE_3;
            // Ð¡Ð±ÑÐ°ÑÑÐ²Ð°ÐµÐ¼ ÑÐµÐºÐ±Ð¾ÐºÑ
            checkbox.checked = false;
        }
    });
    // ÐÐ±ÑÐ°Ð±Ð°ÑÑÐ²Ð°ÐµÐ¼ Ð½Ð°Ð¶Ð°ÑÐ¸Ñ Ð½Ð° ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÐ¸
    radios.forEach(function (currentRadio) {
        currentRadio.addEventListener("change", function (event) {
            // ÐÑÐ±ÑÐ°Ð½Ð½Ð°Ñ ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÐ°
            let radio = event.target;
            // Ð£Ð·Ð½Ð°ÐµÐ¼ ÐºÐ¾Ð½ÐºÑÐµÑÐ½ÑÑ ÑÐ°Ð´Ð¸Ð¾ÐºÐ½Ð¾Ð¿ÐºÑ
            if (radio.value === "r1") {
                // ÐÐ·Ð¼ÐµÐ½ÑÐµÐ¼ Ð´Ð¾Ð¿. ÑÐµÐ½Ñ
                extraPrice = RADIO_1;
            }
            if (radio.value === "r2") {
                extraPrice = RADIO_2;
            }
            if (radio.value === "r3") {
                extraPrice = RADIO_3;
            }
        });
    });
    // ÐÐ±ÑÐ°Ð±Ð°ÑÑÐ²Ð°ÐµÐ¼ Ð½Ð°Ð¶Ð°ÑÐ¸Ðµ ÑÐµÐºÐ±Ð¾ÐºÑÐ°
    checkbox.addEventListener("change", function () {
        // ÐÑÐ»Ð¸ Ð½Ð°Ð¶Ð°Ñ, Ð¼ÐµÐ½ÑÐµÐ¼ Ð´Ð¾Ð¿. ÑÐµÐ½Ñ, Ð¸Ð½Ð°ÑÐµ ÑÐ±ÑÐ°ÑÑÐ²Ð°ÐµÐ¼
        if (checkbox.checked) {
            extraPrice = CHECKBOX;
        } else {
            extraPrice = EXTRA_PRICE;
        }
    });
    // ÐÐ±ÑÐ°Ð±Ð°ÑÑÐ²Ð°ÐµÐ¼ Ð»ÑÐ±ÑÐµ Ð¸Ð·Ð¼ÐµÐ½ÐµÐ½Ð¸Ñ Ð² ÐºÐ°Ð»ÑÐºÑÐ»ÑÑÐ¾ÑÐµ
    // ÐÑÐ»Ð¸ Ð¾Ð½Ð¸ ÐµÑÑÑ, Ð¿ÐµÑÐµÑÑÐ¸ÑÑÐ²Ð°ÐµÐ¼ Ð¸ÑÐ¾Ð³Ð¾Ð²ÑÑ ÑÐµÐ½Ñ
    calc.addEventListener("change", function () {
        // ÐÐ°ÑÐ¸ÑÐ° Ð¾Ñ Ð¾ÑÑÐ¸ÑÐ°ÑÐµÐ»ÑÐ½Ð¾Ð³Ð¾ ÐºÐ¾Ð»Ð¸ÑÐµÑÑÐ²Ð°
        if (myNum.value < 1) {
            myNum.value = 1;
        }
        // ÐÐµÑÐµÑÑÐµÑ ÑÐµÐ·ÑÐ»ÑÑÐ°ÑÐ°
        result = (price + extraPrice) * myNum.value;
        // ÐÑÑÐ°Ð²ÐºÐ° ÑÐµÐ·ÑÐ»ÑÑÐ°ÑÐ° Ð² HTML
        resultSpan.innerHTML = result;
    });
});